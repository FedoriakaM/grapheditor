using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using GraphEditor.Commands;
using GraphEditor.Converters;

namespace GraphEditor
{
    /// <summary>
    /// Interaction logic for GraphCanvas.xaml
    /// </summary>
    public partial class GraphCanvas : Canvas
    {
        //For moving nodes
        private Thickness _nodeOriginalCoordinates;
        private Node _draggedNode;
        private Point _prevCoordinates;

        //For adding edges
        private Edge _tempEdge;
        private int _clicksPerformed;

        //For node renaming
        private Node _renamingNode;

        //For edge renaming
        private Edge _renamingEdge;

        //For selection
        private Point _selectionStartPoint;
        private bool _dragMode;
        private List<Thickness> _oldMargins;
        private List<Node> _nodes;

        public static readonly DependencyProperty ModeProperty = DependencyProperty.Register("Mode", typeof(int), typeof(GraphCanvas), new FrameworkPropertyMetadata(new int()));

        public int Mode
        {
            get { return (int)GetValue(ModeProperty); }
            set 
            {
                SetValue(ModeProperty, value);
            }
        }

        public static readonly DependencyProperty IsOrientedProperty = DependencyProperty.Register("IsOriented", typeof(bool?), typeof(GraphCanvas), new FrameworkPropertyMetadata(new bool?()));
        public bool? IsOriented
        {
            get { return (bool?)GetValue(IsOrientedProperty); }
            set
            {
                SetValue(IsOrientedProperty, value);
            }
        }

        public UndoRedoStack<GraphCanvas> _undoRedoStack = new UndoRedoStack<GraphCanvas>();

        public void CancelAllOperations()
        {
            _draggedNode = null;
            Children.Remove(_tempEdge);
            _tempEdge = null;
            EditBox.Text = string.Empty;
            EditBox.Visibility = Visibility.Hidden;
            if (_renamingNode != null)
            {
                _renamingNode.NodeLabel.Visibility = Visibility.Visible;
                _renamingNode = null;
            }
            if (_renamingEdge != null)
            {
                _renamingEdge.EdgeCost.Visibility = Visibility.Visible;
                _renamingEdge = null;
            }
            _clicksPerformed = 0;
        }

        public void ClearSelection()
        {
            foreach (Node node in Children.OfType<Node>())
            {
                node.NodeView.Stroke = (Brush)FindResource("PrimaryHueDarkBrush");
            }

            foreach (Edge edge in Children.OfType<Edge>())
            {
                edge.EdgeLine.Stroke = (Brush)FindResource("PrimaryHueDarkBrush");
                edge.Arrow1.Stroke = (Brush)FindResource("PrimaryHueDarkBrush");
                edge.Arrow2.Stroke = (Brush)FindResource("PrimaryHueDarkBrush");
            }
        }

        public GraphCanvas()
        {
            InitializeComponent();

            Cursor = Cursors.Arrow;
        }

        private bool KeyModifiersDown
        {
            get { return Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt) || Keyboard.IsKeyDown(Key.RightCtrl); }
        }

        public void DeleteSelected()
        {
            List<UIElement> elements = new List<UIElement>();
            foreach (Node node in Children.OfType<Node>())
                if (node.NodeView.Stroke.Equals((Brush)FindResource("PrimaryHueLightBrush")))
                    elements.Add(node);

            foreach (Edge edge in Children.OfType<Edge>())
                if (edge.EdgeLine.Stroke.Equals((Brush)FindResource("PrimaryHueLightBrush")))
                    elements.Add(edge);

            if (elements.Count > 0)
                _undoRedoStack.Do(new DeleteGroupCommand(elements), this);
        }

        public void SelectAll()
        {
            foreach (Node node in Children.OfType<Node>())
            {
                node.NodeView.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
            }
            foreach(Edge edge in Children.OfType<Edge>())
            {
                edge.EdgeLine.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                edge.Arrow1.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                edge.Arrow2.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
            }
        }

        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!KeyModifiersDown)
            {
                _prevCoordinates = Mouse.GetPosition(this);

                switch (Mode)
                {
                    case -1:
                        {
                            _draggedNode = Children.OfType<Node>().Where(item => item.IsMouseOver == true).FirstOrDefault();
                            if (_draggedNode != null)
                                _nodeOriginalCoordinates = new Thickness(_draggedNode.Margin.Left, _draggedNode.Margin.Top, 0, 0);
                        }
                        break;
                    case 0:
                        {
                            if (Children.OfType<Node>().Where(item => item.IsMouseOver == true).FirstOrDefault() == null)
                            {
                                Point p = Mouse.GetPosition(this);
                                Node tempNode = new Node();
                                tempNode.SetValue(Canvas.ZIndexProperty, 2);
                                tempNode.Margin = new Thickness(p.X - tempNode.Width / 2, p.Y - tempNode.Height / 2, 0, 0);
                                int nextDigit = 0;
                                var newNodes = Children.OfType<Node>().Where(item => item.NodeLabel.Content.ToString().StartsWith("New Node")).LastOrDefault();
                                if (newNodes != null)
                                {
                                    string[] split = newNodes.NodeLabel.Content.ToString().Split(' ');
                                    Int32.TryParse(split[split.Length - 1], out nextDigit);
                                }
                                tempNode.NodeLabel.Content = "New Node " + (nextDigit + 1).ToString();
                                _undoRedoStack.Do(new AddNodeCommand(tempNode), this);
                            }
                        }
                        break;
                    case 1:
                        {
                            if (e.RightButton == MouseButtonState.Pressed)
                            {
                                _clicksPerformed = 0;
                                Children.Remove(_tempEdge);
                                _tempEdge = null;
                            }
                            else
                            {
                                if (_clicksPerformed == 0)
                                {
                                    _tempEdge = new Edge();
                                    _tempEdge.SetValue(Canvas.ZIndexProperty, 1);
                                    _tempEdge.StartNode = Children.OfType<Node>().Where(item => item.IsMouseOver == true).FirstOrDefault();
                                    if (_tempEdge.StartNode != null)
                                    {
                                        _tempEdge.EdgeLine.X1 = _tempEdge.StartNode.Margin.Left + _tempEdge.StartNode.Width / 2;
                                        _tempEdge.EdgeLine.Y1 = _tempEdge.StartNode.Margin.Top + _tempEdge.StartNode.Height / 2;
                                        _tempEdge.EdgeLine.X2 = Mouse.GetPosition(this).X;
                                        _tempEdge.EdgeLine.Y2 = Mouse.GetPosition(this).Y;
                                        Children.Add(_tempEdge);
                                        _clicksPerformed = 1;
                                    }
                                }
                                else
                                {
                                    var possibleEndNode = Children.OfType<Node>().Where(item => item.IsMouseOver == true).FirstOrDefault();
                                    if (!Children.OfType<Edge>().Where(item => !item.Equals(_tempEdge) && ((item.StartNode.Equals(_tempEdge.StartNode) && item.EndNode.Equals(possibleEndNode))
                                        || (item.StartNode.Equals(possibleEndNode) && item.EndNode.Equals(_tempEdge.StartNode)))).Any())
                                    {
                                        _tempEdge.EndNode = possibleEndNode;
                                        if (_tempEdge.EndNode != null && !_tempEdge.EndNode.Equals(_tempEdge.StartNode))
                                        {
                                            _tempEdge.EdgeLine.X2 = _tempEdge.EndNode.Margin.Left + _tempEdge.EndNode.Width / 2;
                                            _tempEdge.EdgeLine.Y2 = _tempEdge.EndNode.Margin.Top + _tempEdge.EndNode.Height / 2;
                                            _tempEdge.EdgeCost.Content = "1";
                                            _tempEdge.AddBindings();
                                            _tempEdge.Arrow1.SetBinding(Line.VisibilityProperty, new Binding("IsOriented")
                                            {
                                                Source = this,
                                                Converter = new BoolToVisibilityConverter(),
                                                NotifyOnSourceUpdated = true
                                            });
                                            _tempEdge.Arrow2.SetBinding(Line.VisibilityProperty, new Binding("IsOriented")
                                            {
                                                Source = this,
                                                Converter = new BoolToVisibilityConverter(),
                                                NotifyOnSourceUpdated = true
                                            });
                                            _undoRedoStack.Do(new AddEdgeCommand(_tempEdge), this);
                                            _clicksPerformed = 0;
                                            _tempEdge = null;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case 2:
                        {
                            var element = Children.OfType<UIElement>().Where(item => item.IsMouseOver == true).FirstOrDefault();
                            if (element != null)
                            {
                                if (element.GetType().Equals(typeof(Edge)))
                                {
                                    _undoRedoStack.Do(new DeleteEdgeCommand((Edge)element), this);
                                }
                                if (element.GetType().Equals(typeof(Node)))
                                {
                                    _undoRedoStack.Do(new DeleteNodeCommand((Node)element), this);
                                }
                            }
                        }
                        break;
                    case 3:
                        {
                            var element = Children.OfType<UIElement>().Where(item => item.IsMouseOver == true).FirstOrDefault();

                            if (_renamingNode != null)
                            {
                                if (EditBox.Text != string.Empty && !Children.OfType<Node>().Where(item => item.NodeLabel.Content.Equals(EditBox.Text.Trim())).Any() && !EditBox.Text.StartsWith("New Node"))
                                {
                                    _undoRedoStack.Do(new RenameNodeCommand(_renamingNode, _renamingNode.NodeLabel.Content as string, EditBox.Text), this);
                                }
                                _renamingNode.NodeLabel.Visibility = Visibility.Visible;
                                _renamingNode = null;
                                EditBox.Text = string.Empty;
                                EditBox.Visibility = Visibility.Hidden;
                            }

                            if (_renamingEdge != null)
                            {
                                int number = 0;
                                if (Int32.TryParse(EditBox.Text, out number))
                                {
                                    _undoRedoStack.Do(new RenameEdgeCommand(_renamingEdge, _renamingEdge.EdgeCost.Content as string, EditBox.Text), this);
                                }
                                _renamingEdge.EdgeCost.Visibility = Visibility.Visible;
                                _renamingEdge = null;
                                EditBox.Text = string.Empty;
                                EditBox.Visibility = Visibility.Hidden;
                            }

                            if (element != null && element.GetType().Equals(typeof(Node)))
                            {
                                _renamingNode = (Node)element;
                                EditBox.Visibility = Visibility.Visible;
                                _renamingNode.NodeLabel.Visibility = Visibility.Hidden;
                                EditBox.Margin = new Thickness(_renamingNode.Margin.Left, _renamingNode.Margin.Top + _renamingNode.Height / 2, 0, 0);
                            }

                            if (element != null && element.GetType().Equals(typeof(Edge)))
                            {
                                _renamingEdge = (Edge)element;
                                EditBox.Visibility = Visibility.Visible;
                                _renamingEdge.EdgeCost.Visibility = Visibility.Hidden;
                                EditBox.Margin = new Thickness(_renamingEdge.EdgeCost.Margin.Left, _renamingEdge.EdgeCost.Margin.Top - _renamingEdge.EdgeCost.ActualHeight / 2, 0, 0);
                            }
                        }
                        break;
                    case 4:
                        {
                            var element = Children.OfType<UIElement>().Where(item => item.IsMouseOver == true).FirstOrDefault();
                            if (element == null)
                            {
                                ClearSelection();
                                SelectionRectangle.Visibility = Visibility.Visible;
                                SelectionRectangle.Margin = new Thickness(Mouse.GetPosition(this).X, Mouse.GetPosition(this).Y, 0, 0);
                                _selectionStartPoint = Mouse.GetPosition(this);
                            }
                            else if (element.GetType().Equals(typeof(Node)))
                            {
                                if (!(element as Node).NodeView.Stroke.Equals((Brush)FindResource("PrimaryHueLightBrush")))
                                {
                                    ClearSelection();
                                    (element as Node).NodeView.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                                    foreach (Edge edge in Children.OfType<Edge>())
                                    {
                                        if (edge.StartNode.Equals((element as Node)) || edge.EndNode.Equals(element as Node))
                                        {
                                            edge.EdgeLine.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                                            edge.Arrow1.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                                            edge.Arrow2.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                                        }
                                    }
                                }
                                _dragMode = true;
                                _nodes = new List<Node>();
                                _oldMargins = new List<Thickness>();
                                foreach (Node node in Children.OfType<Node>())
                                {
                                    if (node.NodeView.Stroke.Equals((Brush)FindResource("PrimaryHueLightBrush")))
                                    {
                                        _nodes.Add(node);
                                        _oldMargins.Add(node.Margin);
                                    }
                                }
                            }
                            else
                            {
                                ClearSelection();
                                (element as Edge).EdgeLine.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                                (element as Edge).Arrow1.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                                (element as Edge).Arrow2.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                            }
                        }
                        break;
                }
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            switch (Mode)
            {
                case -1:
                    {
                        if (e.LeftButton == MouseButtonState.Pressed && _draggedNode != null)
                        {
                            Point p = Mouse.GetPosition(this);
                            int deltaX = (int)(p.X - _prevCoordinates.X);
                            int deltaY = (int)(p.Y - _prevCoordinates.Y);
                            _draggedNode.Margin = new Thickness(_draggedNode.Margin.Left + deltaX, _draggedNode.Margin.Top + deltaY, 0, 0);
                        }
                    }
                    break;
                case 0:
                    {

                    }
                    break;
                case 1:
                    {
                        if (_clicksPerformed == 1)
                        {
                            _tempEdge.EdgeLine.X2 = Mouse.GetPosition(this).X;
                            _tempEdge.EdgeLine.Y2 = Mouse.GetPosition(this).Y;
                        }
                    }
                    break;
                case 2:
                    {

                    }
                    break;
                case 3:
                    {

                    }
                    break;
                case 4:
                    {
                        if (SelectionRectangle.Visibility == Visibility.Visible && e.LeftButton == MouseButtonState.Pressed)
                        {
                            Thickness newmargin = new Thickness();
                            newmargin.Left = _selectionStartPoint.X < Mouse.GetPosition(this).X ? _selectionStartPoint.X : Mouse.GetPosition(this).X;
                            newmargin.Top = _selectionStartPoint.Y < Mouse.GetPosition(this).Y ? _selectionStartPoint.Y : Mouse.GetPosition(this).Y;
                            SelectionRectangle.Margin = newmargin;
                            SelectionRectangle.Width = Math.Abs(_selectionStartPoint.X - Mouse.GetPosition(this).X);
                            SelectionRectangle.Height = Math.Abs(_selectionStartPoint.Y - Mouse.GetPosition(this).Y);
                        }

                        if (_dragMode)
                        {
                            Point p = Mouse.GetPosition(this);
                            int deltaX = (int)(p.X - _prevCoordinates.X);
                            int deltaY = (int)(p.Y - _prevCoordinates.Y);

                            foreach (Node node in Children.OfType<Node>())
                            {
                                if (node.NodeView.Stroke.Equals((Brush)FindResource("PrimaryHueLightBrush")))
                                    node.Margin = new Thickness(node.Margin.Left + deltaX, node.Margin.Top + deltaY, 0, 0);
                            }
                        }
                    }
                    break;
            }

            _prevCoordinates = Mouse.GetPosition(this);
        }

        private void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            switch (Mode)
            {
                case -1:
                    {
                        if (_draggedNode != null)
                        {
                            _undoRedoStack.Do(new MoveNodeCommand(_draggedNode, _nodeOriginalCoordinates, _draggedNode.Margin), this);
                            _draggedNode = null;
                        }
                    }
                    break;
                case 0:
                    {

                    }
                    break;
                case 1:
                    {

                    }
                    break;
                case 2:
                    {

                    }
                    break;
                case 3:
                    {

                    }
                    break;
                case 4:
                    {
                        foreach (Node node in Children.OfType<Node>())
                        {
                            var r1 = new Rect(SelectionRectangle.Margin.Left, SelectionRectangle.Margin.Top, SelectionRectangle.Width, SelectionRectangle.Height);
                            var r2 = new Rect(node.Margin.Left, node.Margin.Top, node.Width, node.Height);
                            r1.Intersect(r2);
                            if (r1 != Rect.Empty)
                            {
                                node.NodeView.Stroke = (Brush)FindResource("PrimaryHueLightBrush");

                                foreach (Edge edge in Children.OfType<Edge>())
                                {
                                    if (edge.StartNode.Equals(node) || edge.EndNode.Equals(node))
                                    {
                                        edge.EdgeLine.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                                        edge.Arrow1.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                                        edge.Arrow2.Stroke = (Brush)FindResource("PrimaryHueLightBrush");
                                    }
                                }
                            }
                        }

                        SelectionRectangle.Visibility = Visibility.Hidden;
                        SelectionRectangle.Margin = new Thickness();
                        SelectionRectangle.Width = 0;
                        SelectionRectangle.Height = 0;

                        if (_dragMode == true)
                            _undoRedoStack.Do(new MoveGroupCommand(_oldMargins, _nodes), this);
                        _dragMode = false;
                    }
                    break;
            }
        }
    }
}
