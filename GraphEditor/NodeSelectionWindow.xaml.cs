﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphEditor
{
    /// <summary>
    /// Interaction logic for NodeSelectionWindow.xaml
    /// </summary>
    public partial class NodeSelectionWindow : Window
    {
        public string ChosenNode { get; set; }
        public NodeSelectionWindow(GraphCanvas canvas)
        {
            InitializeComponent();

            foreach(Node node in canvas.Children.OfType<Node>())
            {
                Button tempButton = new Button
                {
                    Style = (Style)this.FindResource("MaterialDesignFlatButton"),
                    Content = node.NodeLabel.Content.ToString(),
                    ToolTip = node.NodeLabel.Content.ToString()
                };
                tempButton.Click += NodeButton_Click;
                NodeGrid.Children.Add(tempButton);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void NodeButton_Click(object sender, RoutedEventArgs e)
        {
            ChosenNode = (sender as Button).Content.ToString();
            DialogResult = true;
        }
    }
}
