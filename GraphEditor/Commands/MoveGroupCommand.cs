﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GraphEditor.Commands
{
    public class MoveGroupCommand : ICommand<GraphCanvas>
    {
        private List<Thickness> _oldMargins;
        private List<Thickness> _newMargins;
        private List<Node> _nodes;
        public MoveGroupCommand(List<Thickness> oldMargins, List<Node> nodes)
        {
            _oldMargins = oldMargins;
            _nodes = nodes;
            _newMargins = new List<Thickness>();
            foreach (Node node in nodes)
                _newMargins.Add(node.Margin);
        }
        public void Do(GraphCanvas input)
        {
            for (int i = 0; i < _nodes.Count; i++)
                _nodes[i].Margin = _newMargins[i];
        }

        public void Undo(GraphCanvas input)
        {
            for (int i = 0; i < _nodes.Count; i++)
                _nodes[i].Margin = _oldMargins[i];
        }
    }
}
