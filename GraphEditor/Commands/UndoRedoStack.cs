﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphEditor.Commands
{
    public class UndoRedoStack<T> : INotifyPropertyChanged
    {
        private Stack<ICommand<T>> _Undo;
        private Stack<ICommand<T>> _Redo;

        public event PropertyChangedEventHandler PropertyChanged;

        public int UndoCount
        {
            get
            {
                return _Undo.Count;
            }
        }
        public int RedoCount
        {
            get
            {
                return _Redo.Count;
            }
        }

        public UndoRedoStack()
        {
            Reset();
        }
        public void Reset()
        {
            _Undo = new Stack<ICommand<T>>();
            _Redo = new Stack<ICommand<T>>();
        }

        public void Clear()
        {
            _Undo = new Stack<ICommand<T>>();
            _Redo = new Stack<ICommand<T>>();
            PropertyChanged(this, new PropertyChangedEventArgs(string.Empty));
        }

        public void Do(ICommand<T> cmd, T input)
        {
            cmd.Do(input);
            _Undo.Push(cmd);
            _Redo.Clear();

            PropertyChanged(this, new PropertyChangedEventArgs(string.Empty));
        }
        public void Undo(T input)
        {
            if (_Undo.Count > 0)
            {
                ICommand<T> cmd = _Undo.Pop();
                cmd.Undo(input);
                _Redo.Push(cmd);

                PropertyChanged(this, new PropertyChangedEventArgs(string.Empty));
            }
        }
        public void Redo(T input)
        {
            if (_Redo.Count > 0)
            {
                ICommand<T> cmd = _Redo.Pop();
                cmd.Do(input);
                _Undo.Push(cmd);

                PropertyChanged(this, new PropertyChangedEventArgs(string.Empty));
            }
        }
    }
}
