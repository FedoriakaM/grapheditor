﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphEditor.Commands
{
    public class RenameEdgeCommand : ICommand<GraphCanvas>
    {
        private Edge _edge;
        private string _oldCost;
        private string _newCost;
        public RenameEdgeCommand(Edge edge, string oldCost, string newCost)
        {
            _edge = edge;
            _oldCost = oldCost;
            _newCost = newCost;
        }
        public void Do(GraphCanvas input)
        {
            _edge.EdgeCost.Content = _newCost;
        }

        public void Undo(GraphCanvas input)
        {
            _edge.EdgeCost.Content = _oldCost;
        }
    }
}
