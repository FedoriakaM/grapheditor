﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GraphEditor.Commands
{
    public class MoveNodeCommand : ICommand<GraphCanvas>
    {
        private Thickness _originalCoordinates;
        private Thickness _newCoordinates;
        private Node _node;

        public MoveNodeCommand(Node node, Thickness originalCoordinates, Thickness newCoordinates)
        {
            _node = node;
            _originalCoordinates = originalCoordinates;
            _newCoordinates = newCoordinates;
        }

        public void Do(GraphCanvas input)
        {
            _node.Margin = _newCoordinates;
        }

        public void Undo(GraphCanvas input)
        {
            _node.Margin = _originalCoordinates;
        }
    }
}
