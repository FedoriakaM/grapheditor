﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GraphEditor.Commands
{
    public class DeleteGroupCommand : ICommand<GraphCanvas>
    {
        private List<UIElement> _elements;
        public DeleteGroupCommand(List<UIElement> elements)
        {
            _elements = elements;
        }
        public void Do(GraphCanvas input)
        {
            foreach (UIElement element in _elements)
                input.Children.Remove(element);
        }

        public void Undo(GraphCanvas input)
        {
            foreach (UIElement element in _elements)
                input.Children.Add(element);
        }
    }
}
