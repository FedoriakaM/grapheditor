﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphEditor.Commands
{
    public class DeleteEdgeCommand : ICommand<GraphCanvas>
    {
        private Edge _edge;
        public DeleteEdgeCommand(Edge edge)
        {
            _edge = edge;
        }
        public void Do(GraphCanvas input)
        {
            input.Children.Remove(_edge);
        }

        public void Undo(GraphCanvas input)
        {
            input.Children.Add(_edge);
        }
    }
}
