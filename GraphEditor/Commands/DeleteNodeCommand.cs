﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphEditor.Commands
{
    public class DeleteNodeCommand : ICommand<GraphCanvas>
    {
        public Node _node;
        public List<Edge> _edges;
        public DeleteNodeCommand(Node node)
        {
            _node = node;
        }
        public void Do(GraphCanvas input)
        {
            var edgesToDelete = input.Children.OfType<Edge>().Where(item => (item.StartNode.Equals(_node)) || (item.EndNode.Equals(_node))).ToList();
            _edges = edgesToDelete;
            input.Children.Remove(_node);
            foreach(Edge edge in edgesToDelete)
                input.Children.Remove(edge);
        }

        public void Undo(GraphCanvas input)
        {
            input.Children.Add(_node);
            foreach (Edge edge in _edges)
                input.Children.Add(edge);
        }
    }
}
