﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphEditor.Commands
{
    class AddEdgeCommand : ICommand<GraphCanvas>
    {
        private Edge _edge;
        public AddEdgeCommand(Edge edge)
        {
            _edge = edge;
        }
        public void Do(GraphCanvas input)
        {
            if (!input.Children.Contains(_edge))
                input.Children.Add(_edge);
        }

        public void Undo(GraphCanvas input)
        {
            input.Children.Remove(_edge);
        }
    }
}
