﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphEditor.Commands
{
    public class RenameNodeCommand : ICommand<GraphCanvas>
    {
        private Node _node;
        private string _name;
        private string _oldName;
        public RenameNodeCommand(Node node, string oldName, string name)
        {
            _node = node;
            _oldName = oldName;
            _name = name;
        }
        public void Do(GraphCanvas input)
        {
            _node.NodeLabel.Content = _name;
        }

        public void Undo(GraphCanvas input)
        {
            _node.NodeLabel.Content = _oldName;
        }
    }
}
