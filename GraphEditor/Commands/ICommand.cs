﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphEditor.Commands
{
    public interface ICommand<T>
    {
        void Do(T input);
        void Undo(T input);
    }
}
