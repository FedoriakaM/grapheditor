﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphEditor.Commands
{
    public class AddNodeCommand : ICommand<GraphCanvas>
    {
        private Node _node;
        public AddNodeCommand(Node node)
        {
            _node = node;
        }
        public void Do(GraphCanvas input)
        {
            input.Children.Add(_node);
        }

        public void Undo(GraphCanvas input)
        {
            input.Children.Remove(_node);
        }
    }
}
