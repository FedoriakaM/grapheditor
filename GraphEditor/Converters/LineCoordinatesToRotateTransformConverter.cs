﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace GraphEditor.Converters
{
    public class LineCoordinatesToRotateTransformConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double x = (double)values[0];
            double y = (double)values[1];
            string param = (string)values[2];

            if (param == "F")
                return new RotateTransform { Angle = 30, CenterX = x, CenterY = y };
            else
                return new RotateTransform { Angle = -30, CenterX = x, CenterY = y };
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
