﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace GraphEditor.Converters
{
    public class MarginsToEdgeCostMarginCoverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var startNode = (Thickness)values[0];
            var endNode = (Thickness)values[1];
            var sizeNode = (double)values[2];
            var widthLable = (double)values[3];
            var heightLable = (double)values[4];
            var x1 = startNode.Left + sizeNode / 2;
            var x2 = endNode.Left + sizeNode / 2;
            var y1 = startNode.Top + sizeNode / 2;
            var y2 = endNode.Top + sizeNode / 2;
            return new Thickness((x1 + x2) / 2 - widthLable / 2, (y1 + y2) / 2 - heightLable / 2, 0, 0);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
