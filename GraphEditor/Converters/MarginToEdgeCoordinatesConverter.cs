﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace GraphEditor.Converters
{
    public class MarginToEdgeCoordinatesConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var margin = (Thickness)values[0];
            var stringAxis = (string)values[1];
            var size = (double)values[2];

            if (stringAxis == "Y")
            {
                return margin.Top + size / 2;
            }
            else
            {
                return margin.Left + size / 2;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
