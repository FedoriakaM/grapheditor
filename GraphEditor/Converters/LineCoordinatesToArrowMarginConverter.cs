﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace GraphEditor.Converters
{
    public class LineCoordinatesToArrowMarginConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double x1 = (double)values[0];
            double y1 = (double)values[1];
            double x2 = (double)values[2];
            double y2 = (double)values[3];
            double size = (double)values[4];
            string axis = (string)values[5];

            double deltaX = x2 - x1;
            double deltaY = y2 - y1;

            double distance = Math.Sqrt(Math.Pow(deltaX, 2) + Math.Pow(deltaY, 2));

            double sin = Math.Abs(deltaX) / distance;
            double cos = Math.Abs(deltaY) / distance;

            double marginx = deltaX > 0 ? x2 - sin * size : x2 + sin * size;
            double marginy = deltaY > 0 ? y2 - cos * size : y2 + cos * size;

            if (axis == "Y")
                return marginy;
            else
                return marginx;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
