﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GraphEditor.Converters;

namespace GraphEditor
{
    /// <summary>
    /// Interaction logic for Edge.xaml
    /// </summary>
    public partial class Edge : UserControl
    {
        public Node StartNode { get; set; }
        public Node EndNode { get; set; }
        public Edge()
        {
            InitializeComponent();
        }
        public void AddBindings()
        {
            MultiBinding binding = new MultiBinding();
            binding.Converter = new MarginToEdgeCoordinatesConverter();
            binding.Bindings.Add(new Binding("Margin") { Source = StartNode });
            binding.Bindings.Add(new Binding { Source = "X" });
            binding.Bindings.Add(new Binding("Width") { Source = StartNode });
            binding.NotifyOnSourceUpdated = true;
            EdgeLine.SetBinding(Line.X1Property, binding);

            binding = new MultiBinding();
            binding.Converter = new MarginToEdgeCoordinatesConverter();
            binding.NotifyOnSourceUpdated = true;
            binding.Bindings.Add(new Binding("Margin") { Source = StartNode });
            binding.Bindings.Add(new Binding { Source = "Y" });
            binding.Bindings.Add(new Binding("Height") { Source = StartNode });
            EdgeLine.SetBinding(Line.Y1Property, binding);

            binding = new MultiBinding();
            binding.Converter = new MarginToEdgeCoordinatesConverter();
            binding.NotifyOnSourceUpdated = true;
            binding.Bindings.Add(new Binding("Margin") { Source = EndNode });
            binding.Bindings.Add(new Binding { Source = "X" });
            binding.Bindings.Add(new Binding("Width") { Source = EndNode });
            EdgeLine.SetBinding(Line.X2Property, binding);

            binding = new MultiBinding();
            binding.Converter = new MarginToEdgeCoordinatesConverter();
            binding.NotifyOnSourceUpdated = true;
            binding.Bindings.Add(new Binding("Margin") { Source = EndNode });
            binding.Bindings.Add(new Binding { Source = "Y" });
            binding.Bindings.Add(new Binding("Height") { Source = EndNode });
            EdgeLine.SetBinding(Line.Y2Property, binding);

            binding = new MultiBinding();
            binding.Converter = new MarginsToEdgeCostMarginCoverter();
            binding.NotifyOnSourceUpdated = true;
            binding.Bindings.Add(new Binding("Margin") { Source = StartNode });
            binding.Bindings.Add(new Binding("Margin") { Source = EndNode });
            binding.Bindings.Add(new Binding("Height") { Source = EndNode });
            binding.Bindings.Add(new Binding("Width") { Source = EdgeCost });
            binding.Bindings.Add(new Binding("Height") { Source = EdgeCost });
            EdgeCost.SetBinding(Label.MarginProperty, binding);

            binding = new MultiBinding();
            binding.Converter = new LineCoordinatesToArrowMarginConverter();
            binding.NotifyOnSourceUpdated = true;
            binding.Bindings.Add(new Binding("X1") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("Y1") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("X2") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("Y2") { Source = EdgeLine });
            binding.Bindings.Add(new Binding() { Source = EndNode.NodeView.Width / 2 });
            binding.Bindings.Add(new Binding() { Source = "X" });
            Arrow1.SetBinding(Line.X1Property, binding);

            binding = new MultiBinding();
            binding.Converter = new LineCoordinatesToArrowMarginConverter();
            binding.NotifyOnSourceUpdated = true;
            binding.Bindings.Add(new Binding("X1") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("Y1") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("X2") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("Y2") { Source = EdgeLine });
            binding.Bindings.Add(new Binding() { Source = EndNode.NodeView.Width / 2 });
            binding.Bindings.Add(new Binding() { Source = "Y" });
            Arrow1.SetBinding(Line.Y1Property, binding);

            binding = new MultiBinding();
            binding.Converter = new LineCoordinatesToArrowMarginConverter();
            binding.NotifyOnSourceUpdated = true;
            binding.Bindings.Add(new Binding("X1") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("Y1") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("X2") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("Y2") { Source = EdgeLine });
            binding.Bindings.Add(new Binding() { Source = EndNode.NodeView.Width / 2 + 15 });
            binding.Bindings.Add(new Binding() { Source = "X" });
            Arrow1.SetBinding(Line.X2Property, binding);

            binding = new MultiBinding();
            binding.Converter = new LineCoordinatesToArrowMarginConverter();
            binding.NotifyOnSourceUpdated = true;
            binding.Bindings.Add(new Binding("X1") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("Y1") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("X2") { Source = EdgeLine });
            binding.Bindings.Add(new Binding("Y2") { Source = EdgeLine });
            binding.Bindings.Add(new Binding() { Source = EndNode.NodeView.Width / 2 + 15 });
            binding.Bindings.Add(new Binding() { Source = "Y" });
            Arrow1.SetBinding(Line.Y2Property, binding);

            binding = new MultiBinding();
            binding.Converter = new LineCoordinatesToRotateTransformConverter();
            binding.NotifyOnSourceUpdated = true;
            binding.Bindings.Add(new Binding("X1") { Source = Arrow1 });
            binding.Bindings.Add(new Binding("Y1") { Source = Arrow1 });
            binding.Bindings.Add(new Binding() { Source = "F" });
            Arrow1.SetBinding(Line.RenderTransformProperty, binding);

            Arrow2.SetBinding(Line.X1Property, new Binding("X1") { Source = Arrow1 });
            Arrow2.SetBinding(Line.X2Property, new Binding("X2") { Source = Arrow1 });
            Arrow2.SetBinding(Line.Y1Property, new Binding("Y1") { Source = Arrow1 });
            Arrow2.SetBinding(Line.Y2Property, new Binding("Y2") { Source = Arrow1 });

            binding = new MultiBinding();
            binding.Converter = new LineCoordinatesToRotateTransformConverter();
            binding.NotifyOnSourceUpdated = true;
            binding.Bindings.Add(new Binding("X1") { Source = Arrow2 });
            binding.Bindings.Add(new Binding("Y1") { Source = Arrow2 });
            binding.Bindings.Add(new Binding() { Source = "S" });
            Arrow2.SetBinding(Line.RenderTransformProperty, binding);
        }
    }
}
