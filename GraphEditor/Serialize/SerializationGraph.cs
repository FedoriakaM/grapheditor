﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphEditor
{
    public class SerializationGraph
    {
        public List<SerializationNode> Nodes { get; set; }
        public List<SerializationEdge> Edges { get; set; }
        public bool IsOriented { get; set; }
    }
}
