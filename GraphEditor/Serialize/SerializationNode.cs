﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GraphEditor
{
    public class SerializationNode
    {
        public string Name { get; set; }
        public Thickness Margin { get; set; }
    }
}
