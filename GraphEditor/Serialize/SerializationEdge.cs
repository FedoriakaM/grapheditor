﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphEditor
{
    public class SerializationEdge
    {
        public string StartNode { get; set; }
        public string EndNode { get; set; }
        public string EdgeCost { get; set; }
    }
}
