﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GraphEditor
{
    public static class Algorithms
    {
        //Kruskal
        public static void Kruskal(GraphCanvas graphCanvas, MainWindow mainWindow)
        {
            if (mainWindow.OrientedToggle.IsChecked == true)
            {
                mainWindow.AlgorithmResultBlock.Text = "This algorithm can only be performed on non-oriented graph";
                MaterialDesignThemes.Wpf.DrawerHost.OpenDrawerCommand.Execute(null, mainWindow.MyDrawerHost);
            }
            else
            {
                List<Tuple<int, string, string>> edges = new List<Tuple<int, string, string>>();
                foreach (Edge edge in graphCanvas.Children.OfType<Edge>())
                    edges.Add(Tuple.Create(Int32.Parse(edge.EdgeCost.Content as string), edge.StartNode.NodeLabel.Content as string, edge.EndNode.NodeLabel.Content as string));
                edges = edges.OrderBy(item => item.Item1).ToList();

                int cost = 0;
                List<Tuple<string, string>> res = new List<Tuple<string, string>>();

                List<string> nodes = graphCanvas.Children.OfType<Node>().Select(item => item.NodeLabel.Content as string).ToList();
                int[] tree_id = new int[nodes.Count];
                for (int i = 0; i < nodes.Count; i++)
                    tree_id[i] = i;

                for (int i = 0; i < edges.Count; i++)
                {
                    string a = edges[i].Item2;
                    string b = edges[i].Item3;
                    int l = edges[i].Item1;
                    if (tree_id[nodes.IndexOf(a)] != tree_id[nodes.IndexOf(b)])
                    {
                        cost += l;
                        res.Add(Tuple.Create(a, b));
                        int old_id = tree_id[nodes.IndexOf(b)];
                        int new_id = tree_id[nodes.IndexOf(a)];
                        for (int j = 0; j < nodes.Count; j++)
                            if (tree_id[j] == old_id)
                                tree_id[j] = new_id;
                    }
                }

                foreach (Tuple<string, string> tuple in res)
                {
                    graphCanvas.Children.OfType<Node>().Where(item => item.NodeLabel.Content.ToString().Equals(tuple.Item1)).First().NodeView.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                    graphCanvas.Children.OfType<Node>().Where(item => item.NodeLabel.Content.ToString().Equals(tuple.Item2)).First().NodeView.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                    graphCanvas.Children.OfType<Edge>().Where(item => (item.StartNode.NodeLabel.Content.ToString().Equals(tuple.Item1) && item.EndNode.NodeLabel.Content.ToString().Equals(tuple.Item2)) ||
                        (item.StartNode.NodeLabel.Content.ToString().Equals(tuple.Item2) && item.EndNode.NodeLabel.Content.ToString().Equals(tuple.Item1))).First().EdgeLine.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                }

                mainWindow.AlgorithmResultBlock.Text = $"Minimal tree cost is {cost}";
                MaterialDesignThemes.Wpf.DrawerHost.OpenDrawerCommand.Execute(null, mainWindow.MyDrawerHost);
            }
        }
        //Components
        public static List<List<int>> g;
        public static List<bool> used;
        public static List<int> comp;
        public static List<string> names;

        public static void DFS(int v)
        {
            used[v] = true;
            comp.Add(v);
            for (int i = 0; i < g[v].Count; i++)
            {
                int to = g[v][i];
                if (!used[to])
                    DFS(to);
            }
        }

        public static void Find_Comps(GraphCanvas input, MainWindow mainWindow)
        {
            if (mainWindow.OrientedToggle.IsChecked == true)
            {
                mainWindow.AlgorithmResultBlock.Text = "This algorithm can only be performed on non-oriented graph";
                MaterialDesignThemes.Wpf.DrawerHost.OpenDrawerCommand.Execute(null, mainWindow.MyDrawerHost);
            }
            else
            {
                names = input.Children.OfType<Node>().Select(item => item.NodeLabel.Content as string).ToList();
                g = new List<List<int>>();
                for (int i = 0; i < names.Count; i++)
                {
                    List<int> tempList = new List<int>();
                    var onepiece = input.Children.OfType<Edge>().Where(item => item.StartNode.NodeLabel.Content.ToString().Equals(names[i])).Select(item => names.IndexOf(item.EndNode.NodeLabel.Content.ToString()));
                    var secondpiece = input.Children.OfType<Edge>().Where(item => item.EndNode.NodeLabel.Content.ToString().Equals(names[i])).Select(item => names.IndexOf(item.StartNode.NodeLabel.Content.ToString()));
                    tempList.AddRange(onepiece);
                    tempList.AddRange(secondpiece);
                    g.Add(tempList);
                }

                used = new List<bool>();
                for (int i = 0; i < names.Count; i++)
                    used.Add(false);

                mainWindow.AlgorithmResultBlock.Text = string.Empty;

                int componentNumber = 1;

                for (int i = 0; i < names.Count; i++)
                {
                    if (!used[i])
                    {
                        comp = new List<int>();
                        DFS(i);
                        mainWindow.AlgorithmResultBlock.Text += $"Component {componentNumber}:";
                        componentNumber++;
                        for (int j = 0; j < comp.Count; j++)
                        {
                            if (j != 0)
                                mainWindow.AlgorithmResultBlock.Text += ", " + names[comp[j]];
                            else
                                mainWindow.AlgorithmResultBlock.Text += " " + names[comp[j]];
                        }
                        mainWindow.AlgorithmResultBlock.Text += "\n";
                    }
                }

                MaterialDesignThemes.Wpf.DrawerHost.OpenDrawerCommand.Execute(null, mainWindow.MyDrawerHost);
            }
        }

        //BFS

        public static void BFS(GraphCanvas input, MainWindow mainWindow)
        {
            names = input.Children.OfType<Node>().Select(item => item.NodeLabel.Content as string).ToList();
            g = new List<List<int>>();
            for (int i = 0; i < names.Count; i++)
            {
                List<int> tempList = new List<int>();
                var onepiece = input.Children.OfType<Edge>().Where(item => item.StartNode.NodeLabel.Content.ToString().Equals(names[i])).Select(item => names.IndexOf(item.EndNode.NodeLabel.Content.ToString()));
                var secondpiece = input.Children.OfType<Edge>().Where(item => item.EndNode.NodeLabel.Content.ToString().Equals(names[i])).Select(item => names.IndexOf(item.StartNode.NodeLabel.Content.ToString()));
                if (input.IsOriented == true)
                {
                    tempList.AddRange(onepiece);
                }
                else
                {
                    tempList.AddRange(onepiece);
                    tempList.AddRange(secondpiece);
                }
                g.Add(tempList);
            }

            used = new List<bool>();
            for (int i = 0; i < names.Count; i++)
                used.Add(false);

            Queue<int> q = new Queue<int>();

            var window = new NodeSelectionWindow(input) { Owner = mainWindow, WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner };
            var result = window.ShowDialog();
            if (result != true)
                return;

            q.Enqueue(names.IndexOf(window.ChosenNode));
            used[names.IndexOf(window.ChosenNode)] = true;

            while (!(q.Count == 0))
            {
                int v = q.Peek();
                q.Dequeue();
                for (int i = 0; i < g[v].Count; i++)
                {
                    int to = g[v][i];
                    if (!used[to])
                    {
                        used[to] = true;
                        q.Enqueue(to);

                        input.Children.OfType<Node>().Where(item => item.NodeLabel.Content.ToString().Equals(names[to])).First().NodeView.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                        input.Children.OfType<Node>().Where(item => item.NodeLabel.Content.ToString().Equals(names[v])).First().NodeView.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                        input.Children.OfType<Edge>().Where(item => (item.StartNode.NodeLabel.Content.ToString().Equals(names[to]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[v])) ||
                            (item.StartNode.NodeLabel.Content.ToString().Equals(names[v]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[to]))).First().EdgeLine.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                        input.Children.OfType<Edge>().Where(item => (item.StartNode.NodeLabel.Content.ToString().Equals(names[to]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[v])) ||
                            (item.StartNode.NodeLabel.Content.ToString().Equals(names[v]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[to]))).First().Arrow1.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                        input.Children.OfType<Edge>().Where(item => (item.StartNode.NodeLabel.Content.ToString().Equals(names[to]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[v])) ||
                            (item.StartNode.NodeLabel.Content.ToString().Equals(names[v]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[to]))).First().Arrow2.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                    }
                }
            }

            mainWindow.AlgorithmResultBlock.Text = "BFS traversal is highlighted on the scene";
            MaterialDesignThemes.Wpf.DrawerHost.OpenDrawerCommand.Execute(null, mainWindow.MyDrawerHost);
        }
        
        public static void Dijkstra(GraphCanvas input, MainWindow mainWindow)
        {
            const int INF = Int32.MaxValue;

            List<List<Tuple<int, int>>> g = new List<List<Tuple<int, int>>>();
            List<string> names = input.Children.OfType<Node>().Select(item => item.NodeLabel.Content as string).ToList();

            for (int i = 0; i < names.Count; i++)
            {
                List<Tuple<int, int>> tuples = new List<Tuple<int, int>>();
                var onepiece = input.Children.OfType<Edge>().Where(item => item.StartNode.NodeLabel.Content.ToString().Equals(names[i])).Select(item => names.IndexOf(item.EndNode.NodeLabel.Content.ToString())).ToList();
                var secondpiece = input.Children.OfType<Edge>().Where(item => item.EndNode.NodeLabel.Content.ToString().Equals(names[i])).Select(item => names.IndexOf(item.StartNode.NodeLabel.Content.ToString())).ToList();

                var onepieceWeight = input.Children.OfType<Edge>().Where(item => item.StartNode.NodeLabel.Content.ToString().Equals(names[i])).Select(item => Int32.Parse(item.EdgeCost.Content.ToString())).ToList();
                var secondpieceWeight = input.Children.OfType<Edge>().Where(item => item.EndNode.NodeLabel.Content.ToString().Equals(names[i])).Select(item => Int32.Parse(item.EdgeCost.Content.ToString())).ToList();

                for (int j = 0; j < onepiece.Count; j++)
                {
                    tuples.Add(Tuple.Create(onepiece[j], onepieceWeight[j]));
                }

                if (input.IsOriented != true)
                {
                    for (int j = 0; j < secondpiece.Count; j++)
                    {
                        tuples.Add(Tuple.Create(secondpiece[j], secondpieceWeight[j]));
                    }
                }

                g.Add(tuples);
            }

            List<int> d = new List<int>();
            List<int> p = new List<int>();
            List<bool> u = new List<bool>();

            for (int i = 0; i < names.Count; i++)
            {
                d.Add(INF);
                p.Add(-1);
                u.Add(false);
            }

            var window = new NodeSelectionWindow(input) { Owner = mainWindow, WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner };
            var result = window.ShowDialog();
            if (result != true)
                return;

            string startNode = window.ChosenNode;

            Thread.Sleep(500);
            window = new NodeSelectionWindow(input) { Owner = mainWindow, WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner };
            result = window.ShowDialog();
            if (result != true)
                return;

            string endNode = window.ChosenNode;

            d[names.IndexOf(startNode)] = 0;

            for (int i = 0; i < names.Count; i++)
            {
                int v = -1;
                for (int j = 0; j < names.Count; j++)
                {
                    if (!u[j] && (v == -1 || d[j] < d[v]))
                        v = j;
                }

                if (d[v] == INF)
                    break;

                u[v] = true;

                for (int j = 0; j < g[v].Count; j++)
                {
                    int to = g[v][j].Item1;
                    int len = g[v][j].Item2;
                    if (d[v] + len < d[to])
                    {
                        d[to] = d[v] + len;
                        p[to] = v;
                    }
                }
            }

            if (d[names.IndexOf(endNode)] == INF)
            {
                mainWindow.AlgorithmResultBlock.Text = "There is no path between these two nodes";
                MaterialDesignThemes.Wpf.DrawerHost.OpenDrawerCommand.Execute(null, mainWindow.MyDrawerHost);
            }
            else
            {
                int v = names.IndexOf(endNode);
                while (v != names.IndexOf(startNode))
                {
                    input.Children.OfType<Node>().Where(item => item.NodeLabel.Content.ToString().Equals(names[v])).First().NodeView.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                    input.Children.OfType<Node>().Where(item => item.NodeLabel.Content.ToString().Equals(names[p[v]])).First().NodeView.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                    input.Children.OfType<Edge>().Where(item => (item.StartNode.NodeLabel.Content.ToString().Equals(names[v]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[p[v]])) ||
                        (item.StartNode.NodeLabel.Content.ToString().Equals(names[p[v]]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[v]))).First().EdgeLine.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                    input.Children.OfType<Edge>().Where(item => (item.StartNode.NodeLabel.Content.ToString().Equals(names[v]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[p[v]])) ||
                        (item.StartNode.NodeLabel.Content.ToString().Equals(names[p[v]]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[v]))).First().Arrow1.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");
                    input.Children.OfType<Edge>().Where(item => (item.StartNode.NodeLabel.Content.ToString().Equals(names[v]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[p[v]])) ||
                        (item.StartNode.NodeLabel.Content.ToString().Equals(names[p[v]]) && item.EndNode.NodeLabel.Content.ToString().Equals(names[v]))).First().Arrow2.Stroke = (Brush)mainWindow.FindResource("PrimaryHueLightBrush");

                    v = p[v];
                }

                mainWindow.AlgorithmResultBlock.Text = $"The shortest path has length of {d[names.IndexOf(endNode)]}";
                MaterialDesignThemes.Wpf.DrawerHost.OpenDrawerCommand.Execute(null, mainWindow.MyDrawerHost);
            }
        }
    }
}
