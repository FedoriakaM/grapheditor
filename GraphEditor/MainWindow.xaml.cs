﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GraphEditor.Converters;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace GraphEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _currentFileName;
        private bool _changesMade;

        public MainWindow()
        {
            InitializeComponent();

            CommandBindings.Add(new CommandBinding(ApplicationCommands.Undo, Undo_Click));
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Redo, Redo_Click));
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Save, SaveButton_Click));
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Open, LoadButton_Click));
            CommandBindings.Add(new CommandBinding(ApplicationCommands.New, NewButton_Click));
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete, DeleteSelected));
            CommandBindings.Add(new CommandBinding(ApplicationCommands.SelectAll, SelectAll));

            RoutedCommand newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control | ModifierKeys.Shift));
            CommandBindings.Add(new CommandBinding(newCmd, SaveAsButton_Click));

            MainCanvas._undoRedoStack.PropertyChanged += _undoRedoStack_PropertyChanged;

            _currentFileName = null;
            _changesMade = false;
        }

        private void DeleteSelected(object sender, RoutedEventArgs e)
        {
            if (MainToolBox.SelectedIndex == 4)
                MainCanvas.DeleteSelected();
        }

        private void SelectAll(object sender, RoutedEventArgs e)
        {
            if (MainToolBox.SelectedIndex == 4)
                MainCanvas.SelectAll();
        }

        private void _undoRedoStack_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UndoButton.IsEnabled = MainCanvas._undoRedoStack.UndoCount > 0;
            RedoButton.IsEnabled = MainCanvas._undoRedoStack.RedoCount > 0;

            _changesMade = true;
        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            bool isDark = (sender as ToggleButton).IsChecked == true ? true : false;

            var existingResourceDictionary = Application.Current.Resources.MergedDictionaries
                .Where(rd => rd.Source != null)
                .SingleOrDefault(rd => Regex.Match(rd.Source.OriginalString, @"(\/MaterialDesignThemes.Wpf;component\/Themes\/MaterialDesignTheme\.)((Light)|(Dark))").Success);
            if (existingResourceDictionary == null)
                throw new ApplicationException("Unable to find Light/Dark base theme in Application resources.");

            var source =
                $"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.{(isDark ? "Dark" : "Light")}.xaml";
            var newResourceDictionary = new ResourceDictionary() { Source = new Uri(source) };

            Application.Current.Resources.MergedDictionaries.Remove(existingResourceDictionary);
            Application.Current.Resources.MergedDictionaries.Add(newResourceDictionary);

            var existingMahAppsResourceDictionary = Application.Current.Resources.MergedDictionaries
                .Where(rd => rd.Source != null)
                .SingleOrDefault(rd => Regex.Match(rd.Source.OriginalString, @"(\/MahApps.Metro;component\/Styles\/Accents\/)((BaseLight)|(BaseDark))").Success);
            if (existingMahAppsResourceDictionary == null) return;

            source =
                $"pack://application:,,,/MahApps.Metro;component/Styles/Accents/{(isDark ? "BaseDark" : "BaseLight")}.xaml";
            var newMahAppsResourceDictionary = new ResourceDictionary { Source = new Uri(source) };

            Application.Current.Resources.MergedDictionaries.Remove(existingMahAppsResourceDictionary);
            Application.Current.Resources.MergedDictionaries.Add(newMahAppsResourceDictionary);
        }

        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            MainCanvas._undoRedoStack.Undo(MainCanvas);
        }

        private void Redo_Click(object sender, RoutedEventArgs e)
        {
            MainCanvas._undoRedoStack.Redo(MainCanvas);
        }

        private void ToolBarTray_GotMouseCapture(object sender, MouseEventArgs e)
        {
            if (MainCanvas != null)
            {
                MainCanvas.CancelAllOperations();
                MainCanvas.ClearSelection();
                MaterialDesignThemes.Wpf.DrawerHost.CloseDrawerCommand.Execute(null, MyDrawerHost);
            }
        }

        private void MainToolBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MainCanvas != null)
            {
                MainCanvas.CancelAllOperations();
                MainCanvas.ClearSelection();
                MaterialDesignThemes.Wpf.DrawerHost.CloseDrawerCommand.Execute(null, MyDrawerHost);
            }
        }

        private void KruskalButton_Click(object sender, RoutedEventArgs e)
        {
            Algorithms.Kruskal(MainCanvas, this);
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            MainCanvas.ClearSelection();
        }

        private void ComponentButton_Click(object sender, RoutedEventArgs e)
        {
            Algorithms.Find_Comps(MainCanvas, this);
        }

        private void BFSButton_Click(object sender, RoutedEventArgs e)
        {
            Algorithms.BFS(MainCanvas, this);
        }

        private void DijkstraButton_Click(object sender, RoutedEventArgs e)
        {
            Algorithms.Dijkstra(MainCanvas, this);
        }

        private void SaveAs()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Graph (*.json)|*.json";
            if (saveFileDialog.ShowDialog() == true)
            {
                SerializationGraph graph = new SerializationGraph
                {
                    Nodes = new List<SerializationNode>(),
                    Edges = new List<SerializationEdge>(),
                    IsOriented = MainCanvas.IsOriented == true ? true : false
                };
                foreach (Node node in MainCanvas.Children.OfType<Node>())
                {
                    graph.Nodes.Add(new SerializationNode
                    {
                        Name = node.NodeLabel.Content.ToString(),
                        Margin = node.Margin
                    });
                }
                foreach (Edge edge in MainCanvas.Children.OfType<Edge>())
                {
                    graph.Edges.Add(new SerializationEdge
                    {
                        StartNode = edge.StartNode.NodeLabel.Content.ToString(),
                        EndNode = edge.EndNode.NodeLabel.Content.ToString(),
                        EdgeCost = edge.EdgeCost.Content.ToString()
                    });
                }
                File.WriteAllText(saveFileDialog.FileName, JsonConvert.SerializeObject(graph));
                MessageBox.Show("Saved succesfully", "Save", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                _currentFileName = saveFileDialog.FileName;
                _changesMade = false;
            }
        }

        private void Save()
        {
            if (_currentFileName != null)
            {
                SerializationGraph graph = new SerializationGraph
                {
                    Nodes = new List<SerializationNode>(),
                    Edges = new List<SerializationEdge>(),
                    IsOriented = MainCanvas.IsOriented == true ? true : false
                };
                foreach (Node node in MainCanvas.Children.OfType<Node>())
                {
                    graph.Nodes.Add(new SerializationNode
                    {
                        Name = node.NodeLabel.Content.ToString(),
                        Margin = node.Margin
                    });
                }
                foreach (Edge edge in MainCanvas.Children.OfType<Edge>())
                {
                    graph.Edges.Add(new SerializationEdge
                    {
                        StartNode = edge.StartNode.NodeLabel.Content.ToString(),
                        EndNode = edge.EndNode.NodeLabel.Content.ToString(),
                        EdgeCost = edge.EdgeCost.Content.ToString()
                    });
                }
                File.WriteAllText(_currentFileName, JsonConvert.SerializeObject(graph));
                MessageBox.Show("Saved succesfully", "Save", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                _changesMade = false;
            }
            else
            {
                SaveAs();
            }
        }

        private void CleanGraph()
        {
            var nodes = MainCanvas.Children.OfType<Node>().ToList();
            var edges = MainCanvas.Children.OfType<Edge>().ToList();
            foreach(Node node in nodes)
            {
                MainCanvas.Children.Remove(node);
            }
            foreach(Edge edge in edges)
            {
                MainCanvas.Children.Remove(edge);
            }
            MainCanvas._undoRedoStack.Clear();
        }

        private void Load()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Graph (*.json)|*.json";
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    var graph = JsonConvert.DeserializeObject<SerializationGraph>(File.ReadAllText(openFileDialog.FileName));

                    CleanGraph();
                    OrientedToggle.IsChecked = graph.IsOriented;

                    foreach (SerializationNode node in graph.Nodes)
                    {
                        Node tempNode = new Node();
                        tempNode.Margin = node.Margin;
                        tempNode.NodeLabel.Content = node.Name;
                        tempNode.SetValue(Canvas.ZIndexProperty, 2);
                        MainCanvas.Children.Add(tempNode);
                    }

                    foreach (SerializationEdge edge in graph.Edges)
                    {
                        Edge tempEdge = new Edge();
                        tempEdge.StartNode = MainCanvas.Children.OfType<Node>().Where(item => item.NodeLabel.Content.ToString().Equals(edge.StartNode)).First();
                        tempEdge.EndNode = MainCanvas.Children.OfType<Node>().Where(item => item.NodeLabel.Content.ToString().Equals(edge.EndNode)).First();
                        tempEdge.EdgeCost.Content = edge.EdgeCost;
                        MainCanvas.Children.Add(tempEdge);
                        tempEdge.SetValue(Canvas.ZIndexProperty, 1);
                        tempEdge.AddBindings();

                        tempEdge.Arrow1.SetBinding(Line.VisibilityProperty, new Binding("IsOriented")
                        {
                            Source = MainCanvas,
                            Converter = new BoolToVisibilityConverter(),
                            NotifyOnSourceUpdated = true
                        });
                        tempEdge.Arrow2.SetBinding(Line.VisibilityProperty, new Binding("IsOriented")
                        {
                            Source = MainCanvas,
                            Converter = new BoolToVisibilityConverter(),
                            NotifyOnSourceUpdated = true
                        });
                    }

                    MainToolBox.SelectedIndex = -1;

                    _currentFileName = openFileDialog.FileName;
                    _changesMade = false;
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.ToString());
                }
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            if (_changesMade)
            {
                MessageBoxResult result = MessageBox.Show("If you load a graph from file, your current progress will be lost. Do you want to save current graph first?", "Load", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        {
                            Save();
                            Load();
                        }
                        break;
                    case MessageBoxResult.No:
                        {
                            Load();
                        }
                        break;
                    case MessageBoxResult.Cancel:
                        {

                        }
                        break;
                }
            }
            else
            {
                Load();
            }
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            if (_changesMade)
            {
                MessageBoxResult result = MessageBox.Show("If you create new graph, your current progress will be lost. Do you want to save current graph first?", "Create New", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        {
                            Save();
                            CleanGraph();
                            _currentFileName = null;
                            _changesMade = false;
                            MainToolBox.SelectedIndex = -1;
                        }
                        break;
                    case MessageBoxResult.No:
                        {
                            CleanGraph();
                            _currentFileName = null;
                            _changesMade = false;
                            MainToolBox.SelectedIndex = -1;
                        }
                        break;
                    case MessageBoxResult.Cancel:
                        {

                        }
                        break;
                }
            }
            else
            {
                CleanGraph();
                _currentFileName = null;
                _changesMade = false;
            }
        }

        private void SaveAsButton_Click(object sender, RoutedEventArgs e)
        {
            SaveAs();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_changesMade)
            {
                MessageBoxResult result = MessageBox.Show("If you close the app, your current progress will be lost. Do you want to save current graph first?", "Exit", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        {
                            Save();
                            e.Cancel = false;
                        }
                        break;
                    case MessageBoxResult.No:
                        {
                            e.Cancel = false;
                        }
                        break;
                    case MessageBoxResult.Cancel:
                        {
                            e.Cancel = true;
                        }
                        break;
                }
            }
        }
    }
}
